/*
 ============================================================================
 Name        : Prompt.c
 Author      :
 Version     :
 Copyright   :
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef enum {false, true} bool;

typedef struct no
{
    int num;
    struct no *next;
} noElem;

noElem *first = NULL;
noElem *last = NULL;


bool put(int m)
{
    noElem *new =(noElem *)malloc(sizeof(noElem));

    noElem *ant = NULL;
    noElem *atual = first;

    //Verifica se ha elemento repetido
    while( atual != NULL)
    {
        if(atual->num == m)
        {
            return false;
        }
        atual = atual -> next;

    }
    atual = first;


    // Ajusta o ponteiro para inser��o
    while((atual != NULL)) //&& (atual->num < m))//&& (strcmp(atual->nome,v)<0))
    {
        ant = atual;
        atual = atual->next;
    }

    // Se nao tiver algum elemento, o ponteiro que indica o primeiro elemento da lista recebe esse mesmo elemento
    if( ant == NULL )
    {
        first = new;
    }
    else // Senao, faz a inser�ao do novo elemento. O elemento anterior conecta no novo
    {
        ant->next = new;
    }

    new->num = m; // Atribuicao do valor do novo elemento
    new->next = atual; // O novo elemento conecta com o proximo

    if(atual == NULL){ // // Se estiver no fim da lista, o ponteiro aponta para o ultimo elemento conectado com o novo
        last = new;
    }

    return true;

}

bool Remove( int m )
{
    noElem *ant = NULL;
    noElem *atual = first;

    // Enquanto nao tiver no fim da lista e enquanto nao encontrar o numero. Percorre para o proximo
    while((atual != NULL) && ( atual->num!= m))
    {
        ant = atual;
        atual = atual->next;
    }
    if(atual == NULL) //Se estiver no fim da lista, nao encontrou nada
    {
        return false;
    }
    else
    {
        if(atual == first) // Se for o primeiro elemento a ser removido, move o ponteiro para o proximo
        {
            first = atual-> next;
        }
        else // Se nao for o primeiro elemento, o elemento anterior desconecta do elemento a ser inserido e conecta com o proximo
        {
            ant->next = atual->next;
            if(atual == last) // Se for o ultimo elemento a ser removido, o ponteiro do ultimo elemento aponta para o pen�ltimo.
            {
                last = ant;
            }
        }
        free(atual); // Remove o elemento depois de ter configurado a lista
        return true;
    }
}

void imprime()
{
    noElem * atual = first;
    while(atual != NULL)
    {
        printf("%c ", atual->num);
        atual = atual->next;

    }
    printf("  \n");
}

void imprimePrimeiro()
{
    if(first != NULL){
        printf("%c ", first->num);
    }
    else printf("Lista vazia\n");

    printf("  \n");
}

void imprimeUltimo()
{
    if(last->next == NULL && first != NULL){ // O ultimo elemento aponta para nada (NULL). O primeiro ponteiro garante que a lista nao esta vazia
        printf("%c ", last->num);
    }
    else printf("Lista vazia\n");

    printf("  \n");
}

void get( int m )
{
    int counter = 1;
    noElem *ant = NULL;
    noElem *atual = first;

    // Enquanto nao tiver no fim da lista e enquanto nao encontrar o numero. Percorre para o proximo
    while(counter != m && atual->next != NULL)
    {
        ant = atual;
        atual = atual->next;
        counter ++;
    }
    printf("%c \n",ant->num);

    if(atual == NULL) //Se estiver no fim da lista, nao encontrou nada
    {
        printf("Nao encontrado \n");
    }

}

int main(int argc, char *argv[]) {
	char input[201];

	while(1) {
		printf("prompt> ");
		if (fgets(input, 200, stdin) == NULL) {
			printf("An error ocurred.\n");
			break;
		}

		if (strncmp(input, "exit\n", 5) == 0) {
			printf("Leaving. Good bye.\n");
			break;
		}
        if (strncmp(input, "putxx", 2) == 0) {
            if( put( input[4])){
                imprime();
            }

		}
        if (strncmp(input, "getxx", 2) == 0) {
            get( input[4]);
        }
        if (strncmp(input, "removexx", 5) == 0) {
            if( Remove( input[7])){
                imprime();
            }
        }
        if (strncmp(input, "first", 4) == 0) {
            imprimePrimeiro();
        }
        if (strncmp(input, "last", 3) == 0) {
            imprimeUltimo();
        }
        if (strncmp(input, "list", 3) == 0) {
            imprime();
        }
	}

	return EXIT_SUCCESS;
}
